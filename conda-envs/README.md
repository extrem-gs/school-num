# Create conda-forge environments with Miniforge

Without cloning this repo:

```sh
conda env create --file https://gricad-gitlab.univ-grenoble-alpes.fr/extrem-gs/school-num/-/raw/main/conda-envs/env-main.yml
```

or with a local version of the repo:

```sh
cd [...]/school-num/conda-envs
conda env create --file env-root.yml
```
