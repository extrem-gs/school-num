# School on numerics for the Extrem gradual school

https://www.univ-grenoble-alpes.fr/formation/graduate-school/extrem-1038728.kjsp

Source of the website https://extrem-gs.gricad-pages.univ-grenoble-alpes.fr/school-num

## How to build the website and contribute

Install [PDM], [mdformat] and [black], for example with [pipx] (see
https://fluidhowto.readthedocs.io/en/latest/setup-apps.html).

```sh
pipx install pdm
pipx install mdformat
pipx inject mdformat mdformat-myst
pipx install black
```

Clone the repo and run:

```sh
cd school-num
make install-deps
make
```

Modify the files in `book`. Note that `book/_toc.yml` describes the structure of the
site.

[black]: https://github.com/psf/black
[mdformat]: https://github.com/hukkin/mdformat
[pdm]: https://pdm-project.org
[pipx]: https://github.com/pypa/pipx
