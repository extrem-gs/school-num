# Hardware and parallel computing

## Reminder on computer architecture

(hard disk, memory, CPU, GPU)

Different types of CPU, CPU instructions

## Clusters: architecture and schedulers

Importance of Linux.

Computations ("jobs") are submitted via a software called a scheduler (usually Slurm, PBS
or OAR).
