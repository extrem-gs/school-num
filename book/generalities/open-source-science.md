# Open-source and open-science

## Open-source

- Concept of open-source / proprietary software

- Licences (GPL versus more permissive)

- Surprizing success of open-source and relationship with capitalism/huge companies like
  Microsoft, Google, Apple, Facebook, Amazon, ...

## Few examples of open-source projects

Mostly projects used for science.

- Linux

- Python

- ...

## Open-science

Few words... Platforms for sharing data (Zenodo, ...)
