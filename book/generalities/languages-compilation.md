# Languages and compilation

Different languages, different balances, different characteristics...

## Characteristics

- dynamic / static

- low level / high level

- interpreters / compilers, notions of binaries and libraries.

## Open-source communities and ecosystems

## Few examples of languages

List oriented on scientific computing. Few words per language...

### Python

### C/C++

### Fortran

### Java

### JavaScript/TypeScript

### Rust

### Matlab

### Julia

### Mojo
