---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Bash
  language: bash
  name: bash
---

# First steps with Bash and Linux

Bash is the most standard shell on Unix systems (Linux, MacOS, ...). A shell is a basic
programming language to interact with the computer and launch commands.

There are standard commands (corresponding to programs, i.e. to files) to do many basic
tasks. [This short document](https://files.fosswire.com/2007/08/fwunixref.pdf) lists
several important commands.

In the following, we are going to discover few commands and syntaxes. Let's start by a
command to get the current working directory:

```{code-cell}
pwd
```

We see that we ask `pwd` and we get an answer (as a string of characters).

We see that in Linux, the separator for the path is the slash `/` and the root of the
disk is just `/`. Usually, when at the beginning of an interactive session, we start in

It's very important to understand that in a terminal, you are at a particular level in
the directory tree. You have to know in which directory you are. If you don't know, use
`pwd`!

## Command `cd` to change the working directory

Usually, we need to tell `cd` where we want to go to. One can use a full path (something
like `/home/pierre/Output/Teach/num-methods-turb/book`), a relative path (like
`num-methods-turb/book`) or few special cases (`~` for the home directory, `-` for the
directory previously visited, `..` for the directory containing the directory where we
are).

```{code-cell}
cd ~
pwd
```

```{code-cell}
cd -
```

```{code-cell}
cd ..
pwd
```

```{code-cell}
cd -
```

```{code-cell}
cd ../..
pwd
```

```{code-cell}
cd book
pwd
```

```{code-cell}
cd ..
```

## Command `ls` to list the file in a directory

After moving into a directory, it is very common that one wants to know what is in this
directory. Just use

```{code-cell}
ls
```

```{code-cell}
ls book/part0
```

```{code-cell}
# list only a selection of files
ls book/part0/*.md
```

With `ls`, we are going to learn the concept of options for commands. It's possible to
change the behavior of a command by adding options in the command line, written as `-l`.
Let's see what it gives:

```{code-cell}
ls
```

```{code-cell}
ls -l
```

```{code-cell}
ls -a
```

```{code-cell}
ls -la
```

We can get the help for a command and the list of supported options with `man`. Here, we
only plot the 20 first lines (with the command `head`):

```{code-cell}
man ls | head -20
```

## Commands to create (`mkdir`, `touch`) and remove (`rm`) directories and files

```{code-cell}
mkdir tmp_dir
```

```{code-cell}
ls
```

```{code-cell}
touch tmp_dir/toto.txt
```

```{code-cell}
ls tmp_dir
```

```{code-cell}
rm -rf tmp_dir
```

## Command `which` to tell which file corresponds to a command

```{code-cell}
which ls
```

## Command `echo` to print to the screen

```{code-cell}
echo "toto"
```

## Environment variables

There are several already defined environment variables. The syntax to get the value of
an environment variable is `$` followed by the variable name, for example (`echo` is a
command that prints something):

```{code-cell}
echo $HOME
```

```{code-cell}
echo $PATH
```

We can also define our own variables to change the working environment:

```{code-cell}
export MY_ENV_VAR="Bonjour"
```

```{code-cell}
echo $MY_ENV_VAR
```

## Commands to add something in a file and to display the content of a file

We are going to use `echo` plus some "redirection" syntaxes:

```{code-cell}
mkdir -p /tmp/tmp_intro_bash
cd /tmp/tmp_intro_bash
```

```{code-cell}
echo "toto" > tmp.txt
```

```{code-cell}
ls
```

```{code-cell}
cat tmp.txt
```

```{code-cell}
echo "titi" >> tmp.txt
```

```{code-cell}
cat tmp.txt
```

## Command `mv` to rename or move a file and `cp` to copy a file

```{code-cell}
mv tmp.txt tmp2.txt
```

```{code-cell}
cp tmp2.txt tmp3.txt
```

```{code-cell}
ls
```

```{code-cell}
mkdir other_dir
```

```{code-cell}
mv tmp2.txt other_dir
```

```{code-cell}
ls other_dir
```

```{code-cell}
rm -rf /tmp/tmp_intro_bash
```
