# School on numerics, Extrem gradual school

https://www.univ-grenoble-alpes.fr/formation/graduate-school/extrem-1038728.kjsp

## Organizers and speakers

- Jonathan Ferreira,

- [Mourad Ismail](https://liphy-annuaire.univ-grenoble-alpes.fr/pages_personnelles/mourad_ismail/index.php),

- [Pierre Augier](http://www.legi.grenoble-inp.fr/people/Pierre.Augier/index.html).

## Table of contents

```{tableofcontents}
```
