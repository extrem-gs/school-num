# Software engineering

Good practices...

- Particular for the languages (formatting)
- General (packaging, versioning, testing, optimize when needed, ...)

## Table of contents

```{tableofcontents}
```
