# About software installations

## From source

...

## With package managers

### Based on binary donwloads

#### Generalist tools

- apt for Debian based Linux distributions

- pip from the Python package index (PyPI)

- conda

- Pixi (based on conda-forge)

#### Tools specialised in one language

- npm for Javascript
- pip/PDM/uv for Python
- Cargo for Rust
- ...

### Based on local recompilations

#### Generalist tools

- Spack
- Guix
