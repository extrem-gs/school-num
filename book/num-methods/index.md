# Practicals on few numerical methods

We propose to have few seminars on different numerical methods that can be used in
physics. The presentations can consider a particular scientific subject or a class of
methods. Students will also have time to practice on short projects prepared by the
teachers.

## Table of contents

```{tableofcontents}
```
