# Quantum mechanics and Density Functional Theory

Presentations given by
[Damien Caliste](https://www.mem-lab.fr/en/Pages/Portrait/Damien-Caliste.aspx).

Density Functional Theory (DFT) is a computational quantum mechanical modelling method to
investigate the electronic structure of matter. It is widely used in physics, chemistry
and materials science, and is one of the most popular electronic structure methods thanks
to its good compromise between accuracy and speed. DFT can be considered as a
parameter-free approach as it only requires positions of the atoms that build up the
system to simulate

Taken from https://l_sim.gitlab.io/bigdft-doc/

We will use the code BigDFT.

- https://en.wikipedia.org/wiki/BigDFT
- https://gitlab.com/l_sim/bigdft-suite.git

```sh
conda create -n env-bigdft bigdft-suite
```
