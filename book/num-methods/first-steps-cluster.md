# First steps on a supercomputer

As explained in [](../part0/setup-mesonet.md), we are going to learn how to use a real
supercomputer called Zen.

## Setup a computing environment

We already asked you
[to setup you ssh connexion and a minimalist development environment before the school](../part0/setup-mesonet.md).

You should already be able to log on the login node of Zen and these commands should work
without errors on this machine:

```sh
pipx --version
hg version -v
module load miniforge3
conda activate env-fluidsim-mpi
python --version
fluidcluster-help
```

### Modules

Zen works with modules, which can be used to load environments to use different libraries
and programs.

```sh
module avail
```

which gives something like:

```
--------------------------------- /bxfs/modules/core ----------------------------------
   aocc/4.0.0               gnu-parallel/20230922      nodejs/18.18.2
   aocc/4.1.0        (D)    go/1.21.1                  openjdk/21.0.1
   apptainer/1.2.4          intel-oneapi/2023.2        pgi/19.10.nollvm
   gaussian/g16.C.01        miniconda3/23.5.2.py311    pgi/19.10        (D)
   gcc/13.2.0               miniforge3/24.9.0

  Where:
   D:  Default Module
```

### Compile Fluidsim from source (more advanced)

Here, we show how to use modules to setup an environment to be able to compile packages
from source.

Let us first create an environment with only Python:

```sh
module load miniforge3
conda create -n env-fluidsim-from-src python=3.13 -y
```

We can then activate the environment and load few modules.

```sh
conda activate env-fluidsim-from-src
module load gcc openmpi fftw hdf5
```

The list of modules loaded can be printed with `module list`, which gives

```
Currently Loaded Modules:
  1) miniforge3/24.9.0   2) gcc/13.2.0   3) openmpi/4.1.6   4) fftw/3.3.10   5) hdf5/1.14.2.mpi
```

It can be useful to study a module:

```
module show fftw
module show hdf5/1.14.2.mpi
```

which gives for fftw:

```
$ module show fftw
----------------------------------------------------------------------------------------------------------
   /bxfs/modules/mpi/gcc/13.2.0/openmpi/4.1.6/fftw/3.3.10.lua:
----------------------------------------------------------------------------------------------------------
help([[This module loads the FFTW-3.3.10 library build with gcc-13.2.0 and openmpi/4.1.6
]])
prepend_path("PATH","/bxfs/softwares/fftw/3.3.10-gcc.13.2.0-openmpi.4.1.6/bin")
prepend_path("LIBRARY_PATH","/bxfs/softwares/fftw/3.3.10-gcc.13.2.0-openmpi.4.1.6/lib")
prepend_path("LD_LIBRARY_PATH","/bxfs/softwares/fftw/3.3.10-gcc.13.2.0-openmpi.4.1.6/lib")
prepend_path("INCLUDE","/bxfs/softwares/fftw/3.3.10-gcc.13.2.0-openmpi.4.1.6/include")
prepend_path("CPATH","/bxfs/softwares/fftw/3.3.10-gcc.13.2.0-openmpi.4.1.6/include")
prepend_path("PKG_CONFIG_PATH","/bxfs/softwares/fftw/3.3.10-gcc.13.2.0-openmpi.4.1.6/lib/pkgconfig")
```

Then, we can download the installer script with wget or curl

`````{tab-set}

````{tab-item} With wget

```sh
cd /tmp
rm -f install-fluidsim-stack-from-source.py
wget https://foss.heptapod.net/fluiddyn/fluidsim/-/raw/branch/default/scripts/install-fluidsim-stack-from-source.py
```

````

````{tab-item} With curl (macOS)

```sh
cd /tmp
rm -f install-fluidsim-stack-from-source.py
curl -L -O https://foss.heptapod.net/fluiddyn/fluidsim/-/raw/branch/default/scripts/install-fluidsim-stack-from-source.py
```

````

`````

and execute it from the empty virtual environment

```sh
python install-fluidsim-stack-from-source.py -v
```

After the installation, we can check if h5py is correctly installed with (focus on mpi
support and link to the correct hdf5 lib):

```sh
python -c "import h5py; print(h5py.version.info + f'\nmpi: {h5py.get_config().mpi}')"
```

We can test the fluidsim installation with sequential tests:

```sh
pytest --pyargs fluidsim
```

and with parallel tests:

```sh
mpirun -np 2 pytest --pyargs fluidsim
```

## Launch a job with Slurm (manually)

Computations on supercomputers need to be launched with a scheduler. Mesonet and Zen use
Slurm and a nice and simple
[documentation on Slurm](https://www.mesonet.fr/documentation/user-documentation/category/slurm)
is provided.

Other clusters can use other schedulers, for example PBS or OAR. The different schedulers
have different commands. If you don't remember, one can run the command
`fluidcluster-help` (provided by the Python package Fluiddyn) which prints an help
message adapted for the cluster where it has been run.

### Minimal example

We can start by a minimalist example taken from
https://www.mesonet.fr/documentation/user-documentation/code_form/zen/jobs.

Create a temporary directory:

```sh
mkdir -p ~/tmp/example_slurm_sleep
cd ~/tmp/example_slurm_sleep
```

Create a file `job-sleep600.slurm` in this directory containing:

```sh
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --time=00:10:00

hostname
sleep 600
```

This describes a "job", which can be submitted with `sbatch job-sleep600.slurm`. You can
then study the queue de job and the state of this job with the command `squeue`. This job
can be stopped with `scancel`.

### Interactive jobs

When you have a job running on a node, it is then possible to connect to this node with
ssh (`ssh node0xx`). One can then launch interactive computations directly on the node!

However, it is also possible to directly submit an interactive job with something like

```sh
srun --nodes=1 --ntasks-per-node=4 --time=01:00:00 --pty bash -i
```

As soon as a node becomes available, the interactive session begins and you can start to
run programs.

### A Fluidsim simulation

We provide some scripts in the directory
[book/num-methods/mesonet](https://gricad-gitlab.univ-grenoble-alpes.fr/extrem-gs/school-num/-/tree/main/book/num-methods/mesonet).
You can clone the repository and enter into this directory with

```sh
cd ~
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/extrem-gs/school-num.git
cd num-methods-turb/book/num-methods/mesonet
```

We have a Fluidsim script to run a short simulation:

```{literalinclude} mesonet/simul_ns3d_forced_isotropic.py
---
lineno-match:
---
```

And a file `job-fluidsim.slurm`:

```{literalinclude} mesonet/job-fluidsim.slurm
---
lineno-match:
---
```

You can try to launch it with `sbatch job-fluidsim.slurm`.

## Launch a job with Fluiddyn

It is actually much simpler to use a Python script to write and submit your `.slurm`
scripts.

```{literalinclude} mesonet/submit_job_fluidsim_from_src.py
---
lineno-match:
---
```

which can be used with `./submit_job_fluidsim_from_src.py`. This is very powerfull
because one can adapt the script to launch a lot of simulations!

Finally, here is another example using another environment:

```{literalinclude} mesonet/submit_job_fluidsim.py
---
lineno-match:
---
```

You can try to adapt these scripts to launch different simulations.

## Move the data locally with ssh/scp/rsync

When the data is not too big, it can be moved outside of the cluster to be saved and
analyzed.

There are different tools to move data from one computer to another one. Here, we will do
that using ssh as for our connexion and with a very common and useful program called
rsync. One first need to know the path on the cluster of a result, for example I have
results of a simulation saved in a directory
`~/Sim_data/examples/ns3d_96x96x96_V3x3x3_2024-11-22_16-14-57`. From the computer from
which I log on Zen, I can run:

```sh
cd /tmp
rsync -rvz m24075-284@zen.univ-lille.fr:Sim_data/examples/ns3d_96x96x96_V3x3x3_2024-11-22_16-14-57 .
```

```{tip}
You can run `man rsync` to discover what is the meaning of `-rvz`.
```

You can try to download your own data with `rsync`. Then, try to launch few times the
`rsync` command and observe what happens. Does it take the same time after the first
download?
