# Setup computing and development environments with `conda`

In this chapter, we give indications about how to install few tools and how to get the
repository of this school locally on your computer. Please, if you encounter problems,
fill an issue
[here](https://gricad-gitlab.univ-grenoble-alpes.fr/extrem-gs/school-num/issues) to
explain what you did and what are the errors (please copy / paste the error log). It is
also a good exercice to learn how to open an issue in an issue tracker and how to
interact to find a solution to your problem.

## Install good editors

Spyder or VSCode are two good open-source editors (IDE). [Spyder] is specialized on
Python and more oriented on scientific programming. VSCode is more generalist and
lightweight.

Jupyter (lab) is another program that can be used to develop software and notebooks.
Jupyter can be used online (for example
[here](https://gricad-jupyter.univ-grenoble-alpes.fr)) but it is also very convenient to
be able to install it and launch it locally.

It is good if you are able to use these different tools in the situations they are the
most adapted. Finally, you will choose you preferred tools but in practice, it is good to
be able to install and manage these software on different computers.

- https://docs.spyder-ide.org/current/installation.html

- https://code.visualstudio.com/download

- https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html

```{important}
Please try to install Spyder and VSCode from the links above
in the computer you are going to use during the training.
In contrast, we will install JupyterLab with conda in the following.
```

(install-miniforge-and-python)=

## Install Miniforge, Python and Mercurial

First install few software as presented on this page
https://fluidhowto.readthedocs.io/en/latest/python/conda-forge.html.

## Clone this repository with Mercurial

Once everything is installed and setup, you should be able to clone the repository of the
school with:

```bash
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/extrem-gs/school-num.git
```

Please
[tell us](https://gricad-gitlab.univ-grenoble-alpes.fr/extrem-gs/school-num/issues) if it
does not work.

Once the repository is cloned you should have a new directory `school-num` (run the
command `ls` to see it) and you should be able to enter into it with `cd school-num`.

## Install Fluidsim with MPI support

During this school, we are going to use [Fluidsim](https://fluidsim.readthedocs.io). It
can be installed with:

```bash
conda env create --file https://gricad-gitlab.univ-grenoble-alpes.fr/extrem-gs/school-num/-/raw/main/conda-envs/env-fluidsim-mpi.yml
```

Note that the file `env-fluidsim-mpi.yml` contains:

```{literalinclude} ../../conda-envs/env-fluidsim-mpi.yml
```

```{admonition} About this environment

- [MPI](https://en.wikipedia.org/wiki/Message_Passing_Interface) means "Message
Passing Interface", a standard for messages between processes used for
scientific parallel programming.

- [fluidfft](https://fluidfft.readthedocs.io) is a library to compute FFT with
different libraries. We ask for an MPI version.

- `h5py` is the Python wrapper around the famous hdf5 library to write/read .h5
files. We ask for MPI versions of h5py and hdf5.

- We add `ipython` to the environment to be sure that the `ipython` command uses
this environment.

- We add `matplotlib` and `ipympl` to have full support for plotting (with Qt
and in JupyterLab)

```

[spyder]: https://www.spyder-ide.org/
