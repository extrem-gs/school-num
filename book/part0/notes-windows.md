# Notes for Windows users

Until recently, Windows was clearly not optimal for scientific programming and computing.
However, things are moving fast and it is now quite easy to setup a good programming
environment on Windows. However, it takes a bit of time and effort.

Here are few notes to prepare your Windows computer for programming:

- Install [Windows terminal](https://learn.microsoft.com/en-us/windows/terminal/) and use
  it with Powershell! Avoid the old and quite bad "Command Prompt" program.

- Install
  [Python with the Microsoft store](https://apps.microsoft.com/search?query=python). Open
  a terminal, run the command `python` and follow the instructions.

- Install [Ubuntu with WSL](https://learn.microsoft.com/fr-fr/windows/wsl/install).

  You will have a small Linux in your Windows to be able to compile like on
  supercomputers and even run parallel programs using MPI.

- Install [Spyder](https://docs.spyder-ide.org/current/installation.html).

- Install [VSCode](https://code.visualstudio.com/download).

- Install https://github.com/conda-forge/miniforge as explained
  [here](#install-miniforge-and-python).

- Once the command `conda` is available in a terminal,
  [install Mercurial with conda](https://fluidhowto.readthedocs.io/en/latest/mercurial/install-setup.html#with-conda-cross-platform-recommended-for-conda-users-on-windows-linux-and-macos).
