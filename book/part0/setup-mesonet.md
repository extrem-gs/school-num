# First steps with a supercomputer with Mesonet

MesoNET est une fédération de mésocentres de calcul. Les mésocentres sont des structures
publiques (souvent des Unités Mixte de Service) dédiées à fournir des moyens de calculs
tels que des clusters de calcul.

We are going to use a MesoNET supercomputer, so let's see how it works. There are
different websites associated with MesoNET:

- The main website: https://www.mesonet.fr/

- The user documentation: https://www.mesonet.fr/documentation/user-documentation/

- Account creation/management: https://iam.mesonet.fr/

- Management of projects and logins/ssh keys to log on machines: https://acces.mesonet.fr

The steps to use MesoNET services are:

1. Create an account via https://iam.mesonet.fr/

2. Reply to the mail invitation by clicking on the link

3. Login on https://acces.mesonet.fr to manage your logins/ssh keys associated with your
   account and project (see just below)

## Setup your account and connect to Zen

[Zen](https://www.mesonet.fr/documentation/user-documentation/code_form/zen/) is a
supercomputer hosted in Lille. Our goal is to be able to login on the login node of this
supercomputer.

You need to create a ssh key and upload it to the website. Associate the ssh key with a
MesoNET supercomputer (Zen in our case). All this is explained in details
[here](https://www.mesonet.fr/documentation/user-documentation/acces/ssh).

After few hours, the public key that you uploaded on the website should have been copied
in the ~/.ssh directory on the login node of Zen, meaning that you can login with ssh
with the login associated with your project and supercomputer (Zen):

You need to copy this login from the website https://acces.mesonet.fr and you can finally
run (more details
[here](https://www.mesonet.fr/documentation/user-documentation/code_form/zen/connexion)):

```sh
ssh -X m24075-???@zen.univ-lille.fr
```

You should be connected on the login node!

````{tip}

You can add the line

```sh
alias sshzen='ssh -X m24075-???@zen.univ-lille.fr'
```

in your file `~/.bashrc` to be able to log on Zen with the alias `sshzen`.

````

## Presentation of Zen, an example of supercomputer

The technical description of Zen can be found
[here](https://www.mesonet.fr/documentation/user-documentation/code_form/zen/description).

A supercomputer is a set of nodes (individual computers) connected through a network. In
practice, these computers are not like standard personal computers or laptops but stacked
into racks.

In Zen, there are 70 computing nodes (2 CPUs AMD EPYC 9534, 64 cores/CPU, 384 Go RAM
DDR5), 2 similar nodes with a lot of memory (3 To!) and 1 similar visualization node with
2 GPUs.

To launch programs on these nodes, one needs to submit "jobs" via a program called a
scheduler. For Zen, the scheduler is Slurm.

In supercomputer, there are one or few special nodes called login nodes ("frontale" in
French) used for connection, environment preparation (compilation) and job submissions.

## Setup a computing environment

Zen works with modules, which can be used to load environments to use different libraries
and programs.

```sh
module avail
```

which gives something like:

```
--------------------------------- /bxfs/modules/core ----------------------------------
   aocc/4.0.0               gnu-parallel/20230922      nodejs/18.18.2
   aocc/4.1.0        (D)    go/1.21.1                  openjdk/21.0.1
   apptainer/1.2.4          intel-oneapi/2023.2        pgi/19.10.nollvm
   gaussian/g16.C.01        miniconda3/23.5.2.py311    pgi/19.10        (D)
   gcc/13.2.0               miniforge3/24.9.0

  Where:
   D:  Default Module
```

### Install few applications with conda-app and pipx

```sh
module load miniforge3
```

```sh
which conda
```

```sh
conda activate base
pip install conda-app
```

```sh
conda-app install pipx python=3.12
```

```sh
conda-app install mercurial
```

We can open a new session (with ssh) or source the file `~/.bashrc` to get the

```sh
. ~/.bashrc
```

`which hg` should give `~/.local/bin/conda-app-bin/hg`.

Then we can install applications with pipx as described in
https://fluidhowto.readthedocs.io/en/latest/setup-apps.html. For example, for [PDM]:

```sh
pipx install pdm
```

### Create a virtual environment with conda (simple solution)

These commands install few packages in a new environment without compilation.

```sh
module load miniforge3
conda env create --file https://gricad-gitlab.univ-grenoble-alpes.fr/extrem-gs/school-num/-/raw/main/conda-envs/env-fluidsim-mpi.yml
```

Note that the file `env-fluidsim-mpi.yml` contains:

```{literalinclude} ../../conda-envs/env-fluidsim-mpi.yml
```

## Check that everything is setup correctly

Before the school, you should be able to open a terminal on you PC and log on Zen by
running only `sshzen`. Then, the following commands should work without error:

```sh
pipx --version
hg version -v
module load miniforge3
conda activate env-fluidsim-mpi
python --version
fluidcluster-help
```

[pdm]: https://pdm-project.org/
