# Clone the repository of the school

You need to get on your computer the repository of this school, which contains code for
this website and code examples.

Versionning code is one of the important good practices when programming in any language.
For this school, we want you to learn how versionning works. We will present how this can
be done with the program [Mercurial], because it is simpler for the simplest cases and
very powerful (for example used internally at Meta-Facebook, Google, Nokia and Mozilla).
Of course, if you already know and like [Git], use it (but we won't be able to help)!

`````{tab-set}

````{tab-item} With Mercurial client

You should already have installed and setup Mercurial in the [previous page](./install.md).
More details and alternative methods are presented [here](https://fluidhowto.readthedocs.io/en/latest/mercurial/install-setup.html).
The repository can be cloned with:

```sh
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/extrem-gs/school-num.git
```

Please fill an issue
[here](https://gricad-gitlab.univ-grenoble-alpes.fr/extrem-gs/school-num/issues)
if this command finishes with an error.

````

````{tab-item} With Git client

The repository can be cloned with:

```sh
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/extrem-gs/school-num.git
```

````
`````

[git]: https://www.mercurial-scm.org/
[mercurial]: https://www.mercurial-scm.org/
