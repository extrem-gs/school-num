# Notes for macOS users

Open terminal and start by installing the Homebrew packaging manager :
- Follow these instructions to install [Homebrew] (https://brew.sh/).

- You will use python installed from homebrew instead of the native one in macOS.

```sh
brew install python
```

```sh
brew install spyder
```

- Install VSCode macOS application directly form https://code.visualstudio.com/download

- Install [Anaconda](https://gist.github.com/ryanorsinger/7d89ad58901b5590ec3e1f23d7b9f887) by running :

```sh
brew install --cask anaconda
```

- Once the command conda is available in a terminal, install Mercurial with conda.

```sh
brew install pipx
brew install git
```

