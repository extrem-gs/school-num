SHELL := /bin/bash

html: sync
	time pdm run build-book

install-deps: sync

sync:
	pdm sync --clean
	pdm run python -m bash_kernel.install

pdf:
	pdm run build-pdf

clean:
	pdm run clean

format:
	mdformat README.md book/*.md book/*/*.md
	black book

lock:
	pdm lock
